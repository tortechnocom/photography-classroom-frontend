import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { App } from '../../app/app.component';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  get uiLabelMap() {
    return App.uiLabelMap;
  }
}
